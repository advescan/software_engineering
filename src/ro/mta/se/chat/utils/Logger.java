package ro.mta.se.chat.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.*;

public class Logger
{
    public static int level;
    public static String message;
    public static Date resultdate ;
    public static String className;


    private static Logger instance = null;
    private Logger()
    {

    }
    public static Logger getInstance()
    {
        if(instance == null)
        {
            instance = new Logger();
        }
        return instance;
    }


    /**
     * In functie de nivelul global de log, LOGGING_LEVEL, asigneaza level-ul si mesajul corespunzator
     */
    public static void assign() {
        switch (Constants.LOGGING_LEVEL) {
            case 0:
                level = 0;
                message = "OK";
            case 2:
                level = 2;
                message = "VERBOSE";
            case 3:
                level = 3;
                message = "DEBUG";
            case 4:
                level = 4;
                message = "INFO";
            case 5:
                level = 5;
                message = "WARN";
            case 6:
                level = 6;
                message = "ERROR";
        }
    }

    /**
     * In cazul in care nivelul de log cu care functia este apelata este mai mare sau egal in comparatie cu nivelul de log global,
     * functia afiseaza informatii precum timpul, numele clasei curente, nivelul si mesajul corespunzator
     * @param level reprezinta nivelul de log
     * @param message reprezinta mesajul log-ului
     */
    public static void log(int level, String message) {
        if (level >= Constants.LOGGING_LEVEL) {
            assign();
            Logger logger = new Logger();
            long time = System.currentTimeMillis();
            resultdate = new Date(time);
            Logger.level = level;
            className = logger.getClass().getSimpleName();
            Logger.message = message;

            if(Constants.LOG_TO_FILE == false)
            {
                printToConsole();
            }
            else if(Constants.LOG_TO_FILE == true)
            {
                printToFile();
            }
        }
    }

    /**
     * In cazul in care nivelul de log cu care functia este apelata este mai mare sau egal in comparatie cu nivelul de log global,
     * functia afiseaza informatii precum timpul, numele clasei curente, nivelul si mesajul corespunzator
     * @param level reprezinta nivelul de log
     * @param message reprezinta mesajul log-ului
     * @param errorCode reprezinta codul de eroare al log-ului
     */
    public static void log(int level, String message, int errorCode) {
        if (level >= Constants.LOGGING_LEVEL) {
            assign();

            Logger logger = new Logger();
            long time = System.currentTimeMillis();
            resultdate = new Date(time);
            logger.level = level;
            className = logger.getClass().getSimpleName();
            logger.message = message;

            if(Constants.LOG_TO_FILE == false) {
                printToConsole();
                System.out.println("Codul erorii este: " + errorCode);
            }
            else if(Constants.LOG_TO_FILE == true)
            {
                printToFile();
            }

        }
    }

    /**
     * Functia afiseaza informatiile atribuite nivelului curent de log, informatii precum nivelul, mesajul, data curenta
     * si numele clasei curente
     */
    public static void printToConsole()
    {
        System.out.println();
        System.out.println("Nivelul este: " + level);
        System.out.println("Mesajul este: " + message);
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
        System.out.print("Data curenta este: ");
        System.out.println(sdf.format(resultdate));
        System.out.println("Clasa curenta este: " + className);
    }

    public static void printToFile()
    {
        File file = new File(Constants.FILENAME);
        FileWriter writer;
        try {
            writer = new FileWriter(file, true);
            PrintWriter printer = new PrintWriter(writer);
            printer.println();
            printer.append("Nivelul este: " + level);
            printer.println();
            printer.append("Mesajul este: " + message);
            printer.println();
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
            printer.append("Data curenta este: ");
            printer.append(sdf.format((resultdate)));
            printer.println();
            printer.append("Clasa curenta este: " + className);
            printer.println();
            printer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
