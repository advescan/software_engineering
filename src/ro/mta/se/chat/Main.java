package ro.mta.se.chat;

import ro.mta.se.chat.exceptions.ConnectionLostException;
import ro.mta.se.chat.exceptions.NotConnectedException;
import ro.mta.se.chat.utils.Logger;


public class Main {

    /**
     *
     * @param index reprezinta un numar intreg iar in cazul in care el are valoarea "1" este aruncata o exceptie de
     *              tipul ConnectionLostException
     * @throws ConnectionLostException afiseaza mesajul personalizat "ConnectionLostException Message!"
     */
    public static void XHandler_1(int index) throws ConnectionLostException
    {
        if(index==1)
        {
            throw new ConnectionLostException();
        }
    }

    /**
     *
     * @param index reprezinta un numar intreg iar in cazul in care el are valoarea "2" este aruncata o exceptie de
     *              tipul NotConnectedException
     * @throws NotConnectedException afiseaza mesajul personalizat "NotConnectedException Message!"
     */
    public static void XHandler_2(int index) throws NotConnectedException
    {
        if(index==2)
        {
            throw new NotConnectedException();
        }
    }

    public static void main(String[] args) {

        System.out.println("***********************************************Prima tema");
        Logger.log(2, "Verbose Log");
        Logger.log(2, "Verbose Log", 2);
        Logger.log(3, "Debug Log", 3);
//      Logger.log(4, "Info log", 4);
//      Logger.log(5, "Warn Log", 5);
//      Logger.log(6, "Error Log",  6);

        System.out.println();
        System.out.println();

        System.out.println("**********************************************A doua tema");
        System.out.println();
        try
        {
            XHandler_1(0);
            XHandler_2(2);
        }
        catch (ConnectionLostException err)
        {
            System.out.println(err.getMessage());
        }
        catch (NotConnectedException er)
        {
            System.out.println(er.getMessage());
        }

        System.out.println();
        System.out.println();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 3; i++) {
                   // Logger.log("Message1 " + i);
                    Logger.log(i, "Verbose Log");

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        //Logger.log("Thread1 error: " + e.getMessage());
                        Logger.log(i, "Verbose Log",i);

                    }
                }
            }
        });

    }

}